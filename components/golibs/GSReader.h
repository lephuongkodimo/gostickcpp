/*
 * GSReader.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSREADER_H_
#define COMPONENTS_GOLIBS_GSREADER_H_
#include "Adafruit_PN532.h"
#include <vector>
#include <sstream>
#include <string>
#include <esp_log.h>
#include "Arduino.h"
#include <esp_log.h>
#include "GSBlock.h"

static const char *TAG = "GSReader";
// If using the breakout with SPI, define the pins for SPI communication.
#define PN532_SCK  (18)
#define PN532_MOSI (23)
#define PN532_SS   (15)
#define PN532_MISO (19)

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

// Uncomment just _one_ line below depending on how your breakout or shield
// is connected to the Arduino:

// Use this line for a breakout with a software SPI connection (recommended):

class GSReaderCallBacks;
//Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

class GSReader {
	Adafruit_PN532 nfc;
public:

	GSReader() :
			nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS) {
	}
	;

	virtual ~GSReader();
	void init() {
		nfc.begin();
		uint32_t versiondata = nfc.getFirmwareVersion();
		if (!versiondata) {
			//future add warring in here
			Serial.print("Didn't find PN53x board");
			while (1)
				; // halt
		}
		// Got ok data, print it out!
		Serial.print("Found chip PN5");
		Serial.println((versiondata >> 24) & 0xFF, HEX);
		Serial.print("Firmware ver. ");
		Serial.print((versiondata >> 16) & 0xFF, DEC);
		Serial.print('.');
		Serial.println((versiondata >> 8) & 0xFF, DEC);

		// configure board to read RFID tags
		nfc.SAMConfig();
		Serial.println("Waiting for an ISO14443A Card ...");
	}
	void run();
	void stop();
	void setCallBacks(GSReaderCallBacks *mGSReaderCallBack) {
		mGSReaderCallBacks = mGSReaderCallBack;
	}
private:
	bool isBlocks(std::string strTag);//is blocks?
	void write(std::string str);
	std::string read(uint8_t *uid, uint8_t uidLength);
	std::string readBlock(uint8_t *uid, uint8_t uidLength, uint8_t blockNumber);
	GSBlocks mGSBlock;
	GSReaderCallBacks *mGSReaderCallBacks;
	std::vector<uint8_t> vuid_last;
	std::vector<uint8_t> vuid;
	uint8_t _clk, _miso, _mosi, _ss;
	uint8_t cdetec = 0;		//dem so lan xuat hien block

};
class GSReaderCallBacks {
public:
	GSReaderCallBacks() {

	}
	virtual ~GSReaderCallBacks() {

	}

//	virtual void onBlock() = 0;
	virtual void onRead(GSBlocks& mGSBlock) = 0;
};

#endif /* COMPONENTS_GOLIBS_GSREADER_H_ */
