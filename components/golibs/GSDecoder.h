/*
 * GSDecoder.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSDECODER_H_
#define COMPONENTS_GOLIBS_GSDECODER_H_
#include <string>
#include <vector>
#include "GSBlock.h"
#include "GSDefine.h"
#include "GSReader.h"
class GSDecoderCallBacks;
class GSDecoder {
public:
	GSDecoder();
	virtual ~GSDecoder();
	std::string packageBlocks(std::vector<GSBlocks> vbBlocks); //package block to send
	bool isStartBlock(GSBlocks block) {
		return block.getElement(0) == BLOCK_START;
	}
	bool isEndBlock(GSBlocks block) {
		return block.getElement(block.Size() - 1) == BLOCK_END;
	}
	bool isGobot(GSBlocks block) {
		return block.getElement(0) == BLOCK_GOBOT;
	}
	bool isRunBlock(GSBlocks block);
	void processBlocks(GSBlocks block) ;
	/*
	 * Find program in vBlocks START------------END
	 * 0 - 1-2-3-4
	 *
	 */
	std::vector<GSBlocks> getProgram(){
		std::vector<GSBlocks> vgsBlocks ;
		int i = vGSBlocks.size()-1;
		while(vGSBlocks.at(i).whoIam()!=GSBlocks::BLOCKS_NAME::_START){
				i--;
			}
		for( int l = i ; l < vGSBlocks.size() ; l ++){

				if(vGSBlocks.at(l).whoIam() == GSBlocks::BLOCKS_NAME::_END){
					return vgsBlocks;
				}
				vgsBlocks.push_back(vGSBlocks.at(l));
			}
		vgsBlocks.clear();
		vgsBlocks.push_back(vGSBlocks.back());
		return vgsBlocks;
	}
	std::string sToSend(std::vector<GSBlocks> vgsblocks){
		std::string str ="";
		for(int i = 0 ; i < vgsblocks.size() ; i++){
			for (int l = 0; l < vgsblocks[i].Size() ; l ++) {
				str +=vgsblocks[i].getElement(l);
			}
		}
		str= (BLOCK_START + str + BLOCK_END);
		return str;
	}
	void setCallBacks(GSDecoderCallBacks *m_GSDecoderCallBacks) {
		mGSDecoderCallBacks = m_GSDecoderCallBacks;
	}
	std::vector<GSBlocks> getvGSBlocks(){
		return vGSBlocks;
	}
private:
	GSDecoderCallBacks *mGSDecoderCallBacks;
	GSBlocks LastBlock; // last Block
	std::string sToSendBlocks; // to send
	std::vector<GSBlocks> vGSBlocks; //full main program  to send, need convert
};

class GSDecoderCallBacks {
public:
	GSDecoderCallBacks() {

	}
	virtual ~GSDecoderCallBacks() {

	}
	virtual void onStartBlock()=0;
	virtual void onEndBlock()=0;
//	virtual void onRunBlock()=0; //now is not suport
	virtual void onFullBlock()=0; // full block
	virtual void onComplete()=0; //complete program, can run now
	virtual void onGobot()=0; //NFC deteced gobot
};
#endif /* COMPONENTS_GOLIBS_GSDECODER_H_ */
