/*
 * GSCommand.h
 *
 *  Created on: Oct 5, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSCOMMAND_H_
#define COMPONENTS_GOLIBS_GSCOMMAND_H_

namespace GSConfig {

class GSCommand {
public:
	GSCommand();
	virtual ~GSCommand();
};

} /* namespace GSConfig */

#endif /* COMPONENTS_GOLIBS_GSCOMMAND_H_ */
