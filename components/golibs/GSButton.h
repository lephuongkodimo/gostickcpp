/*
 * GSButton.h
 *
 *  Created on: Oct 6, 2018
 *      Author: KING
 */

#ifndef COMPONENTS_GOLIBS_GSBUTTON_H_
#define COMPONENTS_GOLIBS_GSBUTTON_H_
#include "driver/gpio.h"
#include <esp_log.h>
#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "Arduino.h"
#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
#define TIMER_INTERVAL0_SEC   (3.4179) // sample test interval for the first timer
#define TIMER_INTERVAL1_SEC   (5.78)   // sample test interval for the second timer
#define TEST_WITH_RELOAD      1        // testing will be done with auto reload

class Counter {                   // see below for a discussion of
public:
	// why this isn't quite right
	Counter() {
		++count;
	}
	Counter(const Counter&) {
		++count;
	}
	~Counter() {
		--count;
	}

	static size_t howMany() {
		return count;
	}

private:
	static size_t count;
};

// This still goes in an
// implementation file

class GSButtonTimer {
	static double *mTime;
	static timer_idx_t mTimer_idx;
	static uint64_t *ValueTimer;
public:
	void tg0_timer_init(timer_idx_t timer_idx,
			void (*timer_group0_isr)(void*)) {
		mTimer_idx = timer_idx;
		const double timer_interval_sec = TIMER_INTERVAL1_SEC;
		bool auto_reload = TEST_WITH_RELOAD;
		/* Select and initialize basic parameters of the timer */
		timer_config_t config;
		config.divider = TIMER_DIVIDER;
		config.counter_dir = TIMER_COUNT_UP;
		config.counter_en = TIMER_PAUSE;
		config.alarm_en = TIMER_ALARM_EN;
		config.intr_type = TIMER_INTR_LEVEL;
		config.auto_reload = auto_reload;
		timer_init(TIMER_GROUP_0, timer_idx, &config);

		/* Timer's counter will initially start from value below.
		 Also, if auto_reload is set, this value will be automatically reload on alarm */
		timer_set_counter_value(TIMER_GROUP_0, timer_idx, 0x00000000ULL);

		/* Configure the alarm value and the interrupt on alarm. */
		timer_set_alarm_value(TIMER_GROUP_0, timer_idx,
				timer_interval_sec * TIMER_SCALE);
		timer_enable_intr(TIMER_GROUP_0, timer_idx);
		timer_isr_register(TIMER_GROUP_0, timer_idx, timer_group0_isr,
				(void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
		timer_start(TIMER_GROUP_0, timer_idx);

	}
	//clear timer and start timer only once
	void timerStart() {
		TIMERG0.hw_timer[mTimer_idx].config.alarm_en = 1;
	}
	void cleanTimer() {
		TIMERG0.int_clr_timers.t1 = 1;
	}
	void getTimerInSecond(double* time) {
		timer_get_counter_time_sec(TIMER_GROUP_0, mTimer_idx, time);
	}

	void getTimerInValue(uint64_t* timer_val) {
		timer_get_counter_value(TIMER_GROUP_0, TIMER_0, timer_val);
	}

};
class GSButtonDe{
	unsigned long Milis;
	unsigned long lastTime;
	volatile long lastDebounceTime = 0; // the last time the output pin was toggled
	long debounceDelay = 50; // the debounce time; increase if the output flickers
	long timePress = 100;
	long timeHold = 3000;
	QueueHandle_t mxQueue;
public:
	struct sButton {
		uint32_t io_num;
		int state;
	};
	int _cal() {
		if ((millis() - lastDebounceTime) > timeHold) {
			lastDebounceTime = millis();
			return 1;
		} else if ((millis() - lastDebounceTime) > timePress) {
			lastDebounceTime = millis();
			return 0;
		}
		return -1;

	}
	void sendQ(const void *pvItemToQueue){
		xQueueSendFromISR(&mxQueue, &pvItemToQueue, NULL);
	}
	void takeQ(const void *pvItemToQueue) {
		if (xQueueReceive(&mxQueue, &pvItemToQueue, portMAX_DELAY)) {
			ESP_LOGD("BUTTON", "BUTTON : ");

		}
	}
private:
	struct sButton msButton;
};

class GSButtonCallBacks {
public:
	virtual ~GSButtonCallBacks() {
	}
	;
	virtual void onTakePress(void * param) = 0;
	virtual void onTakeHold(void * param) = 0;
};

class GSButton: public GSButtonTimer {
	gpio_num_t io_num;
	Counter c;
	GSButtonCallBacks* mButtonCallBacks;
	volatile long lastDebounceTime = 0; // the last time the output pin was toggled
	long debounceDelay = 50; // the debounce time; increase if the output flickers
	long timePress = 100;
	long timeHold = 3000;
public:
	GSButton();
	virtual ~GSButton();
	GSButton(gpio_num_t gpio_num, gpio_isr_t gpio_isr_handler,
			gpio_int_type_t gpio_intr_type);

	void ButtonISR() {
		this->counter++;
	}
	bool getLevel(){
		return gpio_get_level(this->io_num);
	}
	int TakeButton(){
		if(gpio_get_level(this->io_num)){
		if(TakeHold()) return 1;
		if(TakePress()) return 0;
		}
		return -1;
	}
	bool TakeHold() {
		if (holded) {
			this->pressed = false;
			holded = false;
			mButtonCallBacks->onTakeHold(NULL);
			return true;
		}
		return false;
	}
	bool TakePress() {
		if (pressed) {
			this->holded = false;
			pressed = false;
			this->counter--;
			mButtonCallBacks->onTakePress(NULL);
			return true;
		}
		return false;
	}
	void _gpio_isr() {
		//		clearTimer();
		//		timerStart(TIMER_1);
		//	xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
		this->pressed = true;
	}
	void _gpio_hold(){
		this->holded = true;
	}
	void _timer_group0_isr() {
		//		clearTimer();
		//		xQueueReset(gpio_evt_queue);
//				xQueueSendFromISR(gpio_evt_queue_irq_from_button, &gpio_num, NULL);

	}
	void setCallBacks(GSButtonCallBacks* m_ButtonCallBacks) {
		mButtonCallBacks = m_ButtonCallBacks;
	}
	int _cal() {
		if ((millis() - lastDebounceTime) > timeHold) {
			lastDebounceTime = millis();
			return 1;
		} else if ((millis() - lastDebounceTime) > timePress) {
			lastDebounceTime = millis();
			return 0;
		}
		return -1;

	}
	void _gpio_handle(){
		int state =this->_cal();
		this->setZero();
		if(state==0){
			this->_gpio_isr();
			this->ButtonISR();
		}
		if(state==1) {
			this->_gpio_hold();
		}
	}
	void setZero(){
		 pressed = false;
		 holded = false;
	}
private:
	bool pressed = false;
	bool holded = false;
	volatile int counter = 0;
//	int mButton ;

};
#endif /* COMPONENTS_GOLIBS_GSBUTTON_H_ */
