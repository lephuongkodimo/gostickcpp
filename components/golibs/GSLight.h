/*
 * GSLight.h
 *
 *  Created on: Oct 8, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSLIGHT_H_
#define COMPONENTS_GOLIBS_GSLIGHT_H_

#include "Arduino.h"
#include "driver/gpio.h"
#include <vector>

typedef enum GSLightStatus_t {
	NoConnetGobot = 0,
	ConnetedGobot,
	Startting,
	Shutdown,
	Scanning,
	Connecting,
	Sleep,
	readedStart,
	readedEnd,
//	detectedBlock,
} GSLightStatus_t;
typedef enum ledEvt_t {
	onDetectBlock = 0,
	onConnetedGobot,
	onDisConnetGobot,
	onDetectStartBlock,
	onDetectEndBlock,
} ledEvt_t;
class GSPWM {
	int freq = 5000;
	int _ledChannel = 0;
	int resolution = 8;
	gpio_num_t _io;
public:
	void Setup(gpio_num_t io, int ledChannel) {
		_io = io;
		_ledChannel = ledChannel;
		ledcSetup(_ledChannel, freq, resolution);
		ledcAttachPin(_io, _ledChannel);
	}
	void fade() {
		for (int dutyCycle = 0; dutyCycle <= 255; dutyCycle++) {
			ledcWrite(_ledChannel, dutyCycle);
			delay(7);
		}
	}
};

class GSLed: public GSPWM {
private:
	gpio_num_t _io;
public:
	GSLed() {

	}
	;

	virtual ~GSLed() {

	}
	void init(gpio_num_t io, bool pw) {
		ESP_LOGD("LED", "setup IO: %d ",_io);
		_io=io;
		pinMode(_io, OUTPUT);
		this->off();
		if (pw)
			Setup(_io, 0);
	}

	void on() {
		digitalWrite(this->_io, LOW);
	}
	void off() {
		digitalWrite(this->_io, HIGH);
	}
	void blink() {
		gpio_set_level(this->_io, 0);
		vTaskDelay(100 / portTICK_PERIOD_MS);
		gpio_set_level(this->_io, 1);
	}

};
/*
 * control led light
 */
class GSLight {
	GSLed mLed[3];
	ledEvt_t mLedEvt;
	GSLightStatus_t mStatus;
public:
	GSLight();
	virtual ~GSLight();
	void init() {
		mLed[0].init(GPIO_NUM_12, 0);
		mLed[1].init(GPIO_NUM_14, 0);
		mLed[2].init(GPIO_NUM_13, 0);

	}
	void setStatus(GSLightStatus_t m_Status){
		mStatus=m_Status;
	}
	void show() {
		ESP_LOGD("LED", " Now status is %d", this->mStatus);
		switch (mStatus) {
		case NoConnetGobot:
			for (uint8_t i = 0; i < 3; i++) {
				mLed[i].on();
				vTaskDelay(500 / portTICK_PERIOD_MS);
				mLed[i].off();
			}
			mLed[1].on();
			vTaskDelay(500 / portTICK_PERIOD_MS);
			mLed[1].off();
			break;
		case ConnetedGobot:
			mLed[1].on();
			break;
		case Startting:
		case Shutdown:
		case Scanning:
		case Connecting:
		case Sleep:
		case readedStart:
			mLed[0].on();
			break;
		case readedEnd:
			mLed[1].off();
			mLed[0].off();
			mLed[2].on();
			break;
		default:
			mLed[0].off();
			mLed[1].off();
			mLed[2].off();

		}
	}
	void doLed(ledEvt_t ledEvt) {
		switch (ledEvt) {
		case onDetectBlock:
		case onConnetedGobot:
			mLed[1].blink();
			vTaskDelay(100 / portTICK_PERIOD_MS);
			mLed[1].blink();
			break;
		case onDisConnetGobot:
		case onDetectStartBlock:
			mLed[2].blink();
			break;
		case onDetectEndBlock:
			mLed[2].blink();
			break;
		default:
			break;
		}

	}

};
#endif /* COMPONENTS_GOLIBS_GSLIGHT_H_ */
