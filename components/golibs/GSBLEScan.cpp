/*
 * GSBLEScan.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSBLEScan.h"


GSBLEScan::GSBLEScan() {
	// TODO Auto-generated constructor stub
	BLEDevice::init("");
}

GSBLEScan::~GSBLEScan() {
	// TODO Auto-generated destructor stub
}

void GSBLEScan::start() {
	ESP_LOGD(SCAN_TAG, "Scanning sample starting");
//	BLEDevice::init("");
	pBLEScan = BLEDevice::getScan();
	pBLEScan->setAdvertisedDeviceCallbacks(mpAdvertisedDeviceCallbacks);
	pBLEScan->setActiveScan(true);
	this->scanResults = pBLEScan->start(scanPeriod);
	ESP_LOGI(SCAN_TAG, "We found %d devices", this->scanResults.getCount());
	ESP_LOGD(SCAN_TAG, "Scanning sample ended");
	static int maxRSSI = -255;
	static uint8_t IDmaxRSSI = 0;
	for (int i = 0; i < this->scanResults.getCount(); i++) {
		if (this->scanResults.getDevice(i).getRSSI() > maxRSSI) {
			IDmaxRSSI = i;
			maxRSSI = this->scanResults.getDevice(i).getRSSI();
		}
	}
	ESP_LOGI(SCAN_TAG, "MAX RSSI: %d ", maxRSSI);
	ESP_LOGI(SCAN_TAG, "Divice num: %d", IDmaxRSSI);
}

void GSBLEScan::stop(){
	pBLEScan->stop();
}




