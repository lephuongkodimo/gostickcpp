/*
 * GSDefine.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSDEFINE_H_
#define COMPONENTS_GOLIBS_GSDEFINE_H_
#include <stdint.h>


typedef uint8_t byte;
#define BLOCK_START "100"
#define BLOCK_END "1000"
#define BLOCK_GOBOT "000"



#endif /* COMPONENTS_GOLIBS_GSDEFINE_H_ */
