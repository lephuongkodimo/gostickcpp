/*
 * GSDisplay.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSDISPLAY_H_
#define COMPONENTS_GOLIBS_GSDISPLAY_H_

namespace GSConfig {

class GSDisplay {
public:
	GSDisplay();
	virtual ~GSDisplay();
};

} /* namespace GSConfig */

#endif /* COMPONENTS_GOLIBS_GSDISPLAY_H_ */
