/*
 * GSBLEScan.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSBLESCAN_H_
#define COMPONENTS_GOLIBS_GSBLESCAN_H_
#include <esp_log.h>
#include <BLEScan.h>
#include <BLEDevice.h>
#include <BLEAdvertisedDevice.h>
#include <string>
const char SCAN_TAG[] = "GSBLEScan";
class GSBLEScan {
public:
	GSBLEScan();
	virtual ~GSBLEScan();
	void start();
	void stop();
	/*
	 * Call back when detected divice
	 */
	void GSScanSetAdvertisedDeviceCallbacks(BLEAdvertisedDeviceCallbacks* m_pAdvertisedDeviceCallbacks){
		mpAdvertisedDeviceCallbacks = m_pAdvertisedDeviceCallbacks;
	}
	BLEScanResults getScanResults(){
		return scanResults;
	}
private:
	BLEAdvertisedDeviceCallbacks* mpAdvertisedDeviceCallbacks;
	uint8_t scanPeriod = 5;//Specifies the scan period (in seconds) during periodic scan.
	BLEScanResults scanResults;
	BLEScan* pBLEScan ;

};

#endif /* COMPONENTS_GOLIBS_GSBLESCAN_H_ */
