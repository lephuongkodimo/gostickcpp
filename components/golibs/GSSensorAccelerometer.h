/*
 * GSSensorAccelerometer.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSSENSORACCELEROMETER_H_
#define COMPONENTS_GOLIBS_GSSENSORACCELEROMETER_H_

#include "Arduino.h"
#include <Wire.h>
#include "MPU6050_tockn.h"

class GSSensorAccelerometer {
	MPU6050 mpu6050;
public:
	GSSensorAccelerometer();
	virtual ~GSSensorAccelerometer();
	void init() {
		Wire.begin(21, 22, 100000);
		mpu6050.begin();
		mpu6050.setupInterup();
		mpu6050.calcGyroOffsets(true);
	}
	void update() {
		mpu6050.update();
	}
	void getData() {
		Serial.print("angleX : ");
		Serial.print(mpu6050.getAngleX());
		Serial.print("\tangleY : ");
		Serial.println(mpu6050.getAngleY());
		Serial.print("\tangleZ : ");
		Serial.println(mpu6050.getAngleZ());
	}
private:
};

#endif /* COMPONENTS_GOLIBS_GSSENSORACCELEROMETER_H_ */
