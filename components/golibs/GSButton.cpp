/*
 * GSButton.cpp
 *
 *  Created on: Oct 6, 2018
 *      Author: KING
 */

#include <GSButton.h>

GSButton::GSButton() {
	// TODO Auto-generated constructor stub

}

GSButton::~GSButton() {
	// TODO Auto-generated destructor stub
}
GSButton::GSButton(gpio_num_t gpio_num,
			gpio_isr_t gpio_isr_handler, gpio_int_type_t gpio_intr_type){
			this->io_num = gpio_num;
	gpio_config_t io_conf;
			//interrupt of rising edge
			io_conf.intr_type = gpio_intr_type;
			//bit mask of the pins, use GPIO4/5 here
			io_conf.pin_bit_mask = (1ULL << gpio_num);
			//set as input mode
			io_conf.mode = GPIO_MODE_INPUT;
			//enable pull-up mode
			io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
			gpio_config(&io_conf);

			//install gpio isr service
			gpio_install_isr_service(0);
			//hook isr handler for specific gpio pin
			gpio_isr_handler_add(gpio_num, gpio_isr_handler, (void*) gpio_num);
			//change gpio intrrupt type for one pin
		//	     gpio_set_intr_type(GPIO_INPUT_INT, GPIO_INTR_ANYEDGE);
}

size_t Counter::count = 0;
double *GSButtonTimer::mTime=NULL;
timer_idx_t GSButtonTimer::mTimer_idx=mTimer_idx;
uint64_t *GSButtonTimer::ValueTimer=NULL;

