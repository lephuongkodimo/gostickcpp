/*
 * GSSpeaker.h
 *
 *  Created on: Oct 8, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSSPEAKER_H_
#define COMPONENTS_GOLIBS_GSSPEAKER_H_

namespace GSConfig {

class GSSpeaker {
public:
	GSSpeaker();
	virtual ~GSSpeaker();
};

} /* namespace GSConfig */

#endif /* COMPONENTS_GOLIBS_GSSPEAKER_H_ */
