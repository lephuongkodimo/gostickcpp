/*
 * GSRobotManager.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSROBOTMANAGER_H_
#define COMPONENTS_GOLIBS_GSROBOTMANAGER_H_
#include <GSRobot.h>
#include <vector>
#include <esp_log.h>
#include "GSBLEScan.h"
#include  "Arduino.h"
static const char MAN_TAG[] = "GSBLEScan";
//static const char MAN_TAG[] = "GSRobotManager";
// The remote service we wish to connect to.
static BLEUUID serviceUUID("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
// The characteristic of the remote service we are interested in.
static BLEUUID charUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8");

/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
	/**
	 * Called for each advertising BLE server.
	 */
	void onResult(BLEAdvertisedDevice advertisedDevice) {
		ESP_LOGI(MAN_TAG, "BLE UUID Advertised Device found:");
		ESP_LOGI(MAN_TAG, "%s", advertisedDevice.toString().c_str());
		std::string namegobot = advertisedDevice.getName();
		// We have found a device, let us now see if it contains the service we are looking for.

//		if (advertisedDevice.haveServiceUUID()
//				&& advertisedDevice.getServiceUUID().equals(serviceUUID)) {
//			ESP_LOGI(MAN_TAG, "Found our device!  address: ");
//			//
//			ESP_LOGI(MAN_TAG, "Found our device!  address: ");
//			advertisedDevice.getScan()->stop();
//
//			pServerAddress = new BLEAddress(advertisedDevice.getAddress());
//			doConnect = true;
//
//		} // Found our server

	} // onResult
};
/*
 * Manager scan/connection robot
 */
class GSRobotManager : public BLEAdvertisedDevice{
public:
	GSRobotManager();
	virtual ~GSRobotManager();

	bool connectToRobot(GSRobot& m_GSRobot) ;
	bool isScanning();//Indicates whether GSRobotManager is currently scanning for gobot
	//Returns the current state of the manager
	bool getService(GSRobot& m_GSRobot);
	bool findRobot(GSRobot& mRobot);

	void addRobot();
	void disconnectFromRobot();	//Disconnects an active connection from a robot.
	void startScanningForRobots() {
		//Scans for Robot Nominated periodically based on the given scan period.
	}
	void startScanningAllRobot() ;

private:
	/*
	 * when CallaBack and doConnect == true , we connect Gobbot
	 */
	bool connectToServer(GSRobot m_GSRobot);
	GSBLEScan mGSScan;
	BLEScanResults mScanResults;
	std::vector<GSRobot> mRobotConnected; //list Gobot connected
	std::vector<GSRobot> mRobotFound; //Gobot be found by Scan

};

#endif /* COMPONENTS_GOLIBS_GSROBOTMANAGER_H_ */
