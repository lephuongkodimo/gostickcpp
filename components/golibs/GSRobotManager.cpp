/*
 * GSRobotManager.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSRobotManager.h"



GSRobotManager::GSRobotManager() {
	// TODO Auto-generated constructor stub

}

GSRobotManager::~GSRobotManager() {
	// TODO Auto-generated destructor stub
}
static void notifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
			uint8_t* pData, size_t length, bool isNotify);

bool GSRobotManager::connectToRobot(GSRobot& m_Robot) {
	//connection to a robot.

//		if (isScanning()) mGSScan.stop();


	// If the flag "doConnect" is true then we have scanned for and found the desired
	// BLE Server with which we wish to connect.  Now we connect to it.  Once we are
	// connected we set the connected flag to be true.
	ESP_LOGI(MAN_TAG, "Connecting to the BLE Server.......................");
	ESP_LOGD(MAN_TAG, "m_Robot.doConnect: %d",m_Robot.getDoConnect());
	if (m_Robot.getDoConnect() == true) {
		if (connectToServer(m_Robot)) {
			ESP_LOGI(MAN_TAG, "We are now connected to the BLE Server.");
			m_Robot.setConnectionState(GSConnectionState::GOBOT_CONNECTION_CONNECTED);

		} else {
			ESP_LOGI(MAN_TAG,
					"We have failed to connect to the server; there is nothin more we will do.");
			return false;
		}
		ESP_LOGI(MAN_TAG, "doConnect = false");
		m_Robot.setDoConnect(false);
		return true;
	}
	return false;
};
void GSRobotManager::startScanningAllRobot() {
	mGSScan.GSScanSetAdvertisedDeviceCallbacks(
			new MyAdvertisedDeviceCallbacks());
	ESP_LOGI(MAN_TAG, "BLE Scaning...... ");
	mGSScan.start();
	ESP_LOGI(MAN_TAG, ">>GET  ScanResults");
	mScanResults = mGSScan.getScanResults();
}
bool GSRobotManager::connectToServer(GSRobot m_GSRobot) {
	ESP_LOGD(MAN_TAG, "%s.mAdvertisedDevice.getAddress():",m_GSRobot.getName().c_str());
//	BLEAddress * pServerAddress = new BLEAddress(m_GSRobot.getAddress());
	BLEAddress * pServerAddress = (BLEAddress*)m_GSRobot.getAddress();
	Serial.print("Forming a connection to ");
	Serial.println(pServerAddress->toString().c_str());

	m_GSRobot.mpClient = BLEDevice::createClient();
	Serial.println(" - Created client");

	// Connect to the remove BLE Server.
	m_GSRobot.mpClient->connect(*pServerAddress);
	Serial.println(" - Connected to server");
	return true;
}
bool GSRobotManager::getService(GSRobot& m_GSRobot){

	// Obtain a reference to the service we are after in the remote BLE server.
	m_GSRobot.setRemoteService(m_GSRobot.mpClient->getService(serviceUUID)) ;

	if (m_GSRobot.getRemoteService() == nullptr) {
		ESP_LOGI(MAN_TAG, "Failed to find our service UUID: ");
		Serial.print("Failed to find our service UUID: ");
		Serial.println(serviceUUID.toString().c_str());
		return false;
	}
	Serial.println(" - Found our service");

	// Obtain a reference to the characteristic in the service of the remote BLE server.
	m_GSRobot.setRemoteCharacteristic(m_GSRobot.getRemoteService()->getCharacteristic(charUUID));
	if (m_GSRobot.getRemoteCharacteristic() == nullptr) {
		Serial.print("Failed to find our characteristic UUID: ");
		Serial.println(charUUID.toString().c_str());
		return false;
	}
	Serial.println(" - Found our characteristic");

	// Read the value of the characteristic.
	std::string value = m_GSRobot.getRemoteCharacteristic()->readValue();
	Serial.print("The characteristic value was: ");
	Serial.println(value.c_str());

	m_GSRobot.getRemoteCharacteristic()->registerForNotify(notifyCallback);
	return true;
}
/*
 * find by divice
 */
bool GSRobotManager::findRobot(GSRobot& m_Robot){
	for(uint8_t i =0 ;i < mScanResults.getCount() ; i++){
		ESP_LOGD(MAN_TAG, "mScanResults.getCount %d",mScanResults.getCount());
		ESP_LOGD(MAN_TAG, "getName: %s ",mScanResults.getDevice(i).getName().c_str());
		ESP_LOGD(MAN_TAG, "My RBOTO getName: %s ",m_Robot.getName().c_str());
	if(mScanResults.getDevice(i).getName() ==  m_Robot.getName()){
		ESP_LOGD(MAN_TAG,"	getName:%s " , mScanResults.getDevice(i).getName().c_str());
		ESP_LOGD(MAN_TAG, "m_address from Adv: %s",mScanResults.getDevice(i).getAddress().toString().c_str());
//		m_Robot.mAdvertisedDevice = mScanResults.getDevice(i);
		m_Robot.setAdress(mScanResults.getDevice(i).getAddress().toString());
		ESP_LOGD(MAN_TAG, "m_address from mRobot: %s",m_Robot.mAdvertisedDevice.getAddress().toString().c_str());
		ESP_LOGD(MAN_TAG, "m_Robot.doConnect: %d",m_Robot.getDoConnect());
		m_Robot.setDoConnect(true);
		ESP_LOGD(MAN_TAG, "m_Robot.doConnect: %d",m_Robot.getDoConnect());
		return true;
	}
	}
	return false;
}
/*
 * callback when divice notify
 */
static void notifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
		uint8_t* pData, size_t length, bool isNotify) {
	Serial.print("Notify callback for characteristic ");
	Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
	Serial.print(" of data length ");
	Serial.println(length);
}
