/*
 * GSBlock.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSBLOCK_H_
#define COMPONENTS_GOLIBS_GSBLOCK_H_
#include <string>
#include <vector>
#include "GSDefine.h"


class GSBlocks {
public:
	GSBlocks();
	virtual ~GSBlocks();
	enum BLOCKS_NAME {
		_START,
		_END,
		_GOBOT,
		_FUNC,
	}  ;
	void initBlocks(std::string str);
	int whoIam(){
		if(Blocks[0] == BLOCK_START) return BLOCKS_NAME::_START;
		if(Blocks[0] == BLOCK_END) return BLOCKS_NAME::_END;
		if(Blocks[0] == BLOCK_GOBOT) return BLOCKS_NAME::_GOBOT;
		return BLOCKS_NAME::_FUNC;
	}
	std::string toString(){
		//block chuoi string
		std::string str = Blocks[0];
		for(int i = 1; i< Blocks.size() ; i ++){
		str = str + "," + Blocks[i];
		}
		return str;
	}
	std::string getElement(uint8_t i){
		//return phan tu thu i trong block
		return Blocks[i];
	}
	int Size(){
		//munber  element
		return Blocks.size();
	}
private:

	std::vector<std::string> Blocks;


};



#endif /* COMPONENTS_GOLIBS_GSBLOCK_H_ */
