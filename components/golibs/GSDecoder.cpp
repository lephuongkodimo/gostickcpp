/*
 * GSDecoder.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSDecoder.h"


GSDecoder::GSDecoder() {
	// TODO Auto-generated constructor stub

}

GSDecoder::~GSDecoder() {
	// TODO Auto-generated destructor stub
}
void GSDecoder::processBlocks(GSBlocks block) {
		if (isGobot(block)) {
			mGSDecoderCallBacks->onGobot();
			return;
		}
		if (isStartBlock(block)) {
			vGSBlocks.clear();
			vGSBlocks.push_back(block);
			mGSDecoderCallBacks->onStartBlock();
			return;
		}
		if (isEndBlock(block)) {
			vGSBlocks.push_back(block);
			mGSDecoderCallBacks->onEndBlock();
			return;
		}
		vGSBlocks.push_back(block);

	}

