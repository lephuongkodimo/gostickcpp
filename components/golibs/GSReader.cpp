/*
 * GSReader.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSReader.h"

//GSReader::GSReader() {
//	// TODO Auto-generated constructor stub
//}

GSReader::~GSReader() {
	// TODO Auto-generated destructor stub
}
void GSReader::write(std::string str) {
	uint8_t dataBlock[16];
	memset(dataBlock, 0, 16);
	uint8_t data[16];
	strcpy((char*) dataBlock, str.c_str());
	memcpy(data, dataBlock, sizeof data);
	uint8_t success = nfc.mifareclassic_WriteDataBlock(4, data);
	if (success) {
		ESP_LOGI(TAG, "Done write : %s", str.c_str());
	} else {
		ESP_LOGE(TAG, "Erro write");
	}

}
std::string GSReader::read(uint8_t *uid, uint8_t uidLength) {
	std::string str = "";
	uint8_t blockNumber = 4;
	str = GSReader::readBlock(uid, uidLength, blockNumber);
//	blockNumber = 5 ;
//	str = str + GSReader::readBlock(uid,uidLength,blockNumber);
	return str;
}
std::string GSReader::readBlock(uint8_t *uid, uint8_t uidLength,
		uint8_t blockNumber) {
	std::string str = "";
// We probably have a Mifare Classic card ...
	Serial.println("Seems to be a Mifare Classic card (4 byte UID)");

// Now we need to try to authenticate it for read/write access
// Try with the factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
	Serial.println("Trying to authenticate block 4 with default KEYA value");
	uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

// Start with block 4 (the first block of sector 1) since sector 0
// contains the manufacturer data and it's probably better just
// to leave it alone unless you know what you're doing
	uint8_t success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,
			blockNumber, 0, keya);

	if (success) {
		Serial.println("Sector 1 (Blocks 4..7) has been authenticated");
		uint8_t data[16];
		std::stringstream sd;
		// If you want to write something to block 4 to test with, uncomment
		// the following line and this text should be read back in a minute
		//					memcpy(data, (const uint8_t[]){ 'a', 'd', 'a', 'f', 'r', 'u', 'i', 't', '.', 'c', 'o', 'm', 0, 0, 0, 0 }, sizeof data);
		//					success = nfc.mifareclassic_WriteDataBlock (4, data);

		// Try to read the contents of block 4
		success = nfc.mifareclassic_ReadDataBlock(blockNumber, data);
		if (success) {
			sd << data;
			std::string sdata = sd.str();
			str = sdata;
			// Data seems to have been read ... spit it out
			Serial.println("Reading Block 4:");
			nfc.PrintHexChar(data, 16);
			//ESP_LOGI(LOG_TAG, "STRING: %s", sdata.c_str());
			Serial.println("");

			// Wait a bit before reading the card again
			Serial.println("delay(1000);");

		} else {
			Serial.println(
					"Ooops ... unable to read the requested block.  Try another key?");
		}
	} else {
		Serial.println("Ooops ... authentication failed: Try another key?");
	}
	return str;
}
/*
 * is gsblock
 */
bool GSReader::isBlocks(std::string strTag){
	int index =  strTag.find("(");
	if(index != 0) return false;
	index =  strTag.find(")");
	if(index != 0) return false;
	return true;

}
void GSReader::run(){
	uint8_t success;
	uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 }; // Buffer to store the returned UID
	uint8_t uidLength; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
	unsigned dataArraySize = sizeof(uid) / sizeof(uint8_t);
	// Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
	// 'uid' will be populated with the UID, and uidLength will indicate
	// if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
	success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid,
			&uidLength);
	if (success) {
		vuid.clear();
		copy(&uid[0], &uid[dataArraySize], back_inserter(vuid));
		if (vuid_last == vuid) {
			cdetec++;
			if (cdetec == 4) {
				vuid_last.clear();
				cdetec = 0;
			}
			digitalWrite(12, LOW);delay(100);	digitalWrite(12, HIGH);	delay(100);	digitalWrite(12, LOW);delay(100);digitalWrite(12, HIGH);
			return;
		}
		vuid_last.clear();
		vuid_last = vuid;
		// Display some basic information about the card
		Serial.println("Found an ISO14443A card");
		Serial.print("  UID Length: ");
		Serial.print(uidLength, DEC);
		Serial.println(" bytes");
		Serial.print("  UID Value: ");
		nfc.PrintHex(uid, uidLength);
		Serial.println("");

		if (uidLength == 4) {
			std::string strTag = read(uid,uidLength);
			if(!isBlocks(strTag)) return ;
			mGSBlock.initBlocks(strTag);//Tao 1 GSBlock moi phat hien duoc
			mGSReaderCallBacks->onRead(mGSBlock);//Callback

//			pip();
		}
	}

}
