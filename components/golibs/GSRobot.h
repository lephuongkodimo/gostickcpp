/*
 * GSRobot.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSROBOT_H_
#define COMPONENTS_GOLIBS_GSROBOT_H_
#include <string>
#include <BLEUUID.h>
#include <GSBlock.h>
#include "GSDecoder.h"
#include <BLEDevice.h>
#include <BLEAddress.h>
#include <BLEAdvertisedDevice.h>
enum GSConnectionState{
		GOBOT_CONNECTION_CONNECTED, //0
		GOBOT_CONNECTION_DISCONNECTED,
		GOBOT_CONNECTION_NOCONNECT,
	};
enum GSGobotState{
	GOBOT_GOBOT_RUNNING,

};
class GSRobot :public GSDecoder,BLEAdvertisedDevice{

public:

	GSRobot();
	virtual ~GSRobot();

	int  signalStrength();
	BLEClient* mpClient;//to contro //client cua gostick //only one
	BLEDevice mDevice;//chua lien ket BLE//when connecteed
	BLEAdvertisedDevice mAdvertisedDevice;//chua thong tin quang cao cua robot//when found scan

	bool isConnected();
	bool reboot();
	bool resetState();
	bool equals(BLEAdvertisedDevice advertisedDevice);	//so sanh 2 dia chi hien tai cua gobto va dia chi tu quang cao
	bool getDoConnect();

	void sendCommand(std::vector<GSBlocks>vgsblocks);
	void sendString(std::string str);
	void setConnectionState(GSConnectionState connectionState);
	void setDoConnect (bool bol); //make we sould connect this robot
	void setRemoteService(BLERemoteService* pRemoteService);
	void setRemoteCharacteristic(BLERemoteCharacteristic* pRemoteCharacteristic);
	void setName(std::string nameSet);
	void setAdress(std::string mAddress){
		mstringAddress=mAddress;
		mpServerAddress= new BLEAddress(this->mstringAddress);

	}
	BLEAddress* getAddress();

	BLERemoteCharacteristic* getRemoteCharacteristic();
	BLERemoteService* getRemoteService();
	std::string getName();
	GSConnectionState getConnectionState();


private:
	bool doConnect = false ; //robot need connect
	GSConnectionState mConnectionState = GSConnectionState::GOBOT_CONNECTION_NOCONNECT;
	GSBlocks uuidTag; //id on tag nfc read from Robot over BLE
	std::string nameGobot = "";//name wish found
	std::string firmwareVersion = ""; //found
	std::string serialNumber = "";
	std::string mstringAddress;
	BLEAddress * mpServerAddress;
	// should cretare
	BLERemoteService* mpRemoteService;
	BLERemoteCharacteristic* mpRemoteCharacteristic;






};


#endif /* COMPONENTS_GOLIBS_GSROBOT_H_ */
