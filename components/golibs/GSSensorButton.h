/*
 * GSSensorButton.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSSENSORBUTTON_H_
#define COMPONENTS_GOLIBS_GSSENSORBUTTON_H_

#include "driver/gpio.h"
#include <esp_log.h>
#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
#define TEST_WITH_RELOAD      1        // testing will be done with auto reload
#define TIMER_INTERVAL0_SEC   (3.4179) // sample test interval for the first timer
#define TIMER_INTERVAL1_SEC   (5.78)   // sample test interval for the second timer

#define GPIO_INPUT_INT GPIO_NUM_25
#define GPIO_INPUT_PIN_SEL  (1ULL<<GPIO_INPUT_INT)
#define ESP_INTR_FLAG_DEFAULT 0
typedef struct {
	int type;  // the type of timer's event
	int timer_group;
	int timer_idx;
	uint64_t timer_counter_value;
} timer_event_t;

class GSTimer;
class GSSensorInterup;
class GSSensorButtonCallBacks;



/*
 *
 * static void IRAM_ATTR gpio_isr_handler(void* arg) {

		uint32_t gpio_num = (uint32_t) arg;
		clearTimer();
		timerStart(TIMER_1);
	//	xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);

	}
	static void IRAM_ATTR timer_group0_isr(void* arg) {
		uint32_t gpio_num = (uint32_t) arg;
		clearTimer();
//		xQueueReset(gpio_evt_queue);
		xQueueSendFromISR(gpio_evt_queue_irq_from_button, &gpio_num, NULL);
	}
 *
 */

class GSTimer{
public:

	static void tg0_timer_init(timer_idx_t timer_idx,
	     void (*timer_group0_isr)(void*)  )
	{
		const double timer_interval_sec = TIMER_INTERVAL1_SEC;
		bool auto_reload = TEST_WITH_RELOAD;
	    /* Select and initialize basic parameters of the timer */
	    timer_config_t config;
	    config.divider = TIMER_DIVIDER;
	    config.counter_dir = TIMER_COUNT_UP;
	    config.counter_en = TIMER_PAUSE;
	    config.alarm_en = TIMER_ALARM_EN;
	    config.intr_type = TIMER_INTR_LEVEL;
	    config.auto_reload = auto_reload;
	    timer_init(TIMER_GROUP_0, timer_idx, &config);

	    /* Timer's counter will initially start from value below.
	       Also, if auto_reload is set, this value will be automatically reload on alarm */
	    timer_set_counter_value(TIMER_GROUP_0, timer_idx, 0x00000000ULL);

	    /* Configure the alarm value and the interrupt on alarm. */
	    timer_set_alarm_value(TIMER_GROUP_0, timer_idx, timer_interval_sec * TIMER_SCALE);
	    timer_enable_intr(TIMER_GROUP_0, timer_idx);
	    timer_isr_register(TIMER_GROUP_0, timer_idx,  timer_group0_isr,
	        (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
	    timer_start(TIMER_GROUP_0, timer_idx);

	}
	//clear timer and start timer only once
	static void timerStart(timer_idx_t timer_idx){
		TIMERG0.hw_timer[timer_idx].config.alarm_en = 1;
	}

	static void clearTimer() {
		TIMERG0.int_clr_timers.t1 = 1;
	}

};

class GSSensorInterup : public GSTimer {
public:
	GSSensorInterup() {

	}
	~GSSensorInterup() {

	}
	void initInterup(gpio_isr_t gpio_isr_handler);
	void initInterup(gpio_num_t gpio_num, gpio_isr_t gpio_isr_handler,
			gpio_int_type_t gpio_intr_type, bool add);
	void GSIntrDisable(gpio_num_t gpio_num);
	void GSIntrEnable(gpio_num_t gpio_num);

};

class GSSensorButton :public GSSensorInterup {
public:
	GSSensorInterup* mSensorInterup;
	GSSensorButton();
	virtual ~GSSensorButton();

	int counterPress = 0;
	void setButton(gpio_num_t io,gpio_isr_t gpio_isr_handler, bool de) ;
	void Button();
	void run(){	}
	void setCallBacks(GSSensorButtonCallBacks* m_SensorButtonCallBacks) {
		mSensorButtonCallBacks = m_SensorButtonCallBacks;
	}
	bool isPressed() {
		return pressed;
	}
private:
	int io_num;
	bool enDebounce = false;
	bool pressed = false;

	GSSensorButtonCallBacks* mSensorButtonCallBacks;
};

class GSSensorButtonInterup {
public:
	GSSensorButtonInterup();
	virtual ~GSSensorButtonInterup();
};

class GSSensorButtonCallBacks {
public:
	GSSensorButtonCallBacks() {
	}
	;
	virtual ~GSSensorButtonCallBacks() {
	}
	;
	virtual void onPressed(void *param) = 0;
};


#endif /* COMPONENTS_GOLIBS_GSSENSORBUTTON_H_ */
