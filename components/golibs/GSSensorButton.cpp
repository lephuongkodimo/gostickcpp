/*
 * GSSensorButton.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSSensorButton.h"

GSSensorButton::GSSensorButton() {
	// TODO Auto-generated constructor stub

}

GSSensorButton::~GSSensorButton() {
	// TODO Auto-generated destructor stub
}
/*
 * wait and callback when revice signal from button
 */
void GSSensorButton::Button(){	}


void GSSensorButton::setButton(gpio_num_t io,gpio_isr_t gpio_isr_handler, bool de) {
	io_num = io;
	mSensorInterup->initInterup(io,gpio_isr_handler,GPIO_INTR_POSEDGE,0);
	ESP_LOGI("BUTTON","initInterup");
}


void GSSensorInterup::initInterup(gpio_isr_t gpio_isr_handler) {
	gpio_config_t io_conf;
	//interrupt of rising edge
	io_conf.intr_type = GPIO_INTR_LOW_LEVEL;
	//bit mask of the pins, use GPIO4/5 here
	io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
	//set as input mode
	io_conf.mode = GPIO_MODE_INPUT;
	//enable pull-up mode
	io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
	gpio_config(&io_conf);

	//install gpio isr service
	gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	//hook isr handler for specific gpio pin
	gpio_isr_handler_add(GPIO_INPUT_INT, gpio_isr_handler,
			(void*) GPIO_INPUT_INT);
	//change gpio intrrupt type for one pin
//	     gpio_set_intr_type(GPIO_INPUT_INT, GPIO_INTR_ANYEDGE);

}
void GSSensorInterup::initInterup(gpio_num_t gpio_num,
		gpio_isr_t gpio_isr_handler, gpio_int_type_t gpio_intr_type, bool add) {
	gpio_config_t io_conf;
	//interrupt of rising edge
	io_conf.intr_type = gpio_intr_type;
	//bit mask of the pins, use GPIO4/5 here
	io_conf.pin_bit_mask = (1ULL << gpio_num);
	//set as input mode
	io_conf.mode = GPIO_MODE_INPUT;
	//enable pull-up mode
	io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
	gpio_config(&io_conf);

	//install gpio isr service
	if (!add)
		gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	//hook isr handler for specific gpio pin
	gpio_isr_handler_add(gpio_num, gpio_isr_handler, (void*) gpio_num);
	//change gpio intrrupt type for one pin
//	     gpio_set_intr_type(GPIO_INPUT_INT, GPIO_INTR_ANYEDGE);

}
void GSSensorInterup::GSIntrDisable(gpio_num_t gpio_num) {
	gpio_intr_disable(gpio_num);
}
void GSSensorInterup::GSIntrEnable(gpio_num_t gpio_num) {
	gpio_intr_enable(gpio_num);
}



