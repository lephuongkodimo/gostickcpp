/*
 * GSRobot.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSRobot.h"



GSRobot::GSRobot() {
	// TODO Auto-generated constructor stub

}

GSRobot::~GSRobot() {
	// TODO Auto-generated destructor stub
}


	bool GSRobot::isConnected() {
		//Returns true if robot is connected and ready to receive commands (and currently emitting sensor).
		return mConnectionState == GSConnectionState::GOBOT_CONNECTION_CONNECTED;
	}
	bool GSRobot::reboot(){
		return true;
	}
	bool GSRobot::resetState(){
		return true;
	}
	void GSRobot::sendCommand(std::vector<GSBlocks>vgsblocks){
		GSDecoder::sToSend(vgsblocks);
		//TODO send BLE

	}
	void GSRobot::sendString(std::string str){
		mpRemoteCharacteristic->writeValue(str.c_str(), str.length());
	}
	//so sanh 2 dia chi hien tai cua gobto va dia chi tu quang cao
	bool GSRobot::equals(BLEAdvertisedDevice advertisedDevice){
		return advertisedDevice.getAddress().equals(mDevice.getAddress());
	}

	void GSRobot::setRemoteService(BLERemoteService* pRemoteService){
		 mpRemoteService=pRemoteService;
	}
	BLERemoteService* GSRobot::getRemoteService(){
		return mpRemoteService;
	}
	void GSRobot::setRemoteCharacteristic(BLERemoteCharacteristic* pRemoteCharacteristic){
			mpRemoteCharacteristic = pRemoteCharacteristic;
		}
	BLERemoteCharacteristic* GSRobot::getRemoteCharacteristic(){
		return mpRemoteCharacteristic ;
			}
	void GSRobot::setName(std::string nameSet){
		nameGobot = nameSet;
	}
	std::string GSRobot::getName(){
		//return name
		return nameGobot;
	}
	GSConnectionState GSRobot::getConnectionState(){
		return mConnectionState;
	}
	void GSRobot::setConnectionState(GSConnectionState connectionState){
		mConnectionState = connectionState;
	}
	void GSRobot::setDoConnect (bool bol){
		this->doConnect = bol ;
	}
	bool GSRobot::getDoConnect(){
		return this->doConnect;
	}
	BLEAddress* GSRobot::getAddress() {
//		if(mpServerAddress!=NULL)
		return this->mpServerAddress;
	}
