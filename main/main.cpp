#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <string>
#include <esp_log.h>
#include "driver/gpio.h"
#include <inttypes.h>
#include "Arduino.h"
//#include "esp_system.h"
#include <sstream>

#include "nvs_flash.h"
#include <vector>
#include <Wire.h>
#include <SPI.h>

#include "Adafruit_PN532.h"
#include "MPU6050_tockn.h"

#include "GSReader.h"
#include "GSSensorButton.h"
#include "GSSensorAccelerometer.h"
#include "GSBLEScan.h"
#include "GSRobotManager.h"
#include "GSRobot.h"
#include "GSButton.h"
#include "GSLight.h"
//#include "nvs.h"
extern "C" {

void app_main(void);
}
static xQueueHandle gpio_evt_queue_irq_from_button;//give hanlde when neeed

GSSensorAccelerometer mpu6050;
static GSRobot Gobot1;
static GSRobot Gobot2;
GSLight mLight;

//The Name Gobot interest
static std::string BotName1 = "BOBO";
static std::string BotName2 = "ESP";
int scanTime = 5; //In seconds
static const char LOG_TAG[] = ">>GOSTICK<<";

bool onPip = false;
void pip() {
	digitalWrite(12, LOW);
	digitalWrite(32, LOW);
	delay(111);
	digitalWrite(12, HIGH);
	digitalWrite(32, HIGH);
}
std::string to_string(int i){
	std::stringstream ss;
	ss << i;
	return ss.str();
}
void printfv(std::vector<uint8_t> vdata) {
	for (int i = 0; i < vdata.size(); i++) {
		Serial.print(vdata[i]);
	}
	Serial.println("");

}

/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */

static void bleTask(void *param) {
	GSRobotManager RobotManager;
	RobotManager.startScanningAllRobot();
	Gobot1.setName(BotName1);
//	Gobot2.setName(BotName2);
	int i = 0;
	while (1) {

		if(RobotManager.findRobot(Gobot1)) {
			ESP_LOGI(LOG_TAG, "FINDED GOBOT %s",Gobot1.getName().c_str());
		}
//		if(RobotManager.findRobot(Gobot2)){
//			ESP_LOGI(LOG_TAG, "FINDED GOBOT %s",Gobot2.getName().c_str());
//		}
		if (!Gobot1.isConnected()) {
			ESP_LOGI(LOG_TAG, "TRY CONNECTING......");
			if (RobotManager.findRobot(Gobot1)) {
				ESP_LOGD(MAN_TAG, "m_Robot.doConnect: %d",Gobot1.getDoConnect());
				ESP_LOGD(LOG_TAG, "FINDED GOBOT %s",Gobot1.getName().c_str());
				if (RobotManager.connectToRobot(Gobot1)) {
					ESP_LOGI(LOG_TAG, "CONNECTED %s", BotName1.c_str());
					RobotManager.getService(Gobot1);
					ESP_LOGI(LOG_TAG, "GETSERVICE...");

				} else {
					ESP_LOGI(LOG_TAG, "CONNECT FAILED:  %s", BotName1.c_str());
				}
			} else {
				ESP_LOGI(LOG_TAG, "FAILED:NO FIND ROBOT %s ", BotName1.c_str());
			}
		}
		ESP_LOGI(LOG_TAG, "___________________");
		if(Gobot1.isConnected()){
			std::string mstr = "PIKING : " + to_string(i++);
			ESP_LOGI(LOG_TAG, "SENDING : %s",mstr.c_str());
			Gobot1.sendString(mstr);
		}
		vTaskDelay(2000 / portTICK_PERIOD_MS);

	}

}

/*
 * set call back when press
 */
class MyButtonCallBacks: public GSButtonCallBacks{
public:
	void onTakePress(void * param){
		ESP_LOGI("PRESS:"," Button :PRESS ");
		pip();
	}
	void onTakeHold(void * param){
			ESP_LOGI("PRESS:"," Button : HOLD");
		}

};

static void IRAM_ATTR gpio_isr_handler_num_0(void* arg) ;
GSButton *bt0 = new GSButton(GPIO_NUM_0,gpio_isr_handler_num_0,GPIO_INTR_NEGEDGE) ;
static void IRAM_ATTR gpio_isr_handler_num_0(void* arg) {
//		bt0->_gpio_handle();
	bt0->_gpio_isr();
}
/*
 * ISR when intertup
 */
static void IRAM_ATTR gpio_isr_handler_num_4(void* arg) {
	uint32_t gpio_num = (uint32_t) arg;
	bt0->_gpio_isr();
}
static void imuTask(void* param) {
	gpio_evt_queue_irq_from_button = xQueueCreate(10, sizeof(uint32_t));
	bt0->setCallBacks(new MyButtonCallBacks);
	mpu6050.init();
	int mState;
	GSSensorInterup io4;
	io4.initInterup(GPIO_NUM_4, gpio_isr_handler_num_4, GPIO_INTR_NEGEDGE, 1);
	ESP_LOGI("TIMER:"," _timerStart_");
	bt0->setZero();
	mLight.setStatus(GSLightStatus_t::NoConnetGobot);
	while (1) {
	mLight.show();
		bt0->TakeButton();
//		if (onPip)
//			pip();
//		onPip = false;
//		mpu6050.update();
//		if (!bt0->getLevel()) {
//			mpu6050.getData();
//		}
	}
}
static void nfcTask(void *param) {
	GSReader nfcReader;
	nfcReader.init();
	while (1) {
		nfcReader.run();
	}
}
void app_main(void) {
	Serial.begin(115200);
	ESP_LOGD(LOG_TAG, "initArduino");
	initArduino();
	//setup
	ESP_LOGD(LOG_TAG, "nvs_flash_init");
	ESP_ERROR_CHECK(nvs_flash_init());
	Serial.println("Hello!");
	mLight.init();
	pinMode(26, OUTPUT);					//EN SUPOWER RFID
	pinMode(32, OUTPUT);					//SPEAKER
	pinMode(4, INPUT_PULLUP);

//	pinMode(0, INPUT_PULLUP);					//BUTTON

	digitalWrite(32, HIGH);
	digitalWrite(26, LOW); //low == kick on
//	bleTask(NULL);
	xTaskCreate(&bleTask, "bleTask", 40000, NULL, 2, NULL);
//	vTaskDelay(5000 / portTICK_PERIOD_MS);
//	xTaskCreate(&nfcTask, "nfcTask", 10000, NULL, 2, NULL);
//	xTaskCreate(&imuTask, "imuTask", 10000, NULL, 2, NULL);
} // app_main
